ARG PYTHON_VERSION
FROM python:${PYTHON_VERSION}-slim

ARG POETRY_VERSION
ENV POETRY_HOME=/usr/local

COPY install_poetry.py /install_poetry.py

RUN python3 install_poetry.py
